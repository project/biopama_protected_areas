//This file loads first
var tipTemplate    = '<div class="tooltip biopama-tip" role="tooltip"><div class="arrow biopama-arrow"></div><div class="tooltip-inner biopama-tip-inner"></div></div>';
var tipWideTemplate    = '<div class="tooltip biopama-tip biopama-tip-wide" role="tooltip"><div class="arrow biopama-arrow"></div><div class="tooltip-inner biopama-tip-inner"></div></div>';
var tipTopTemplate = '<div class="tooltip biopama-tip" role="tooltip"><div class="arrow biopama-top-arrow"></div><div class="tooltip-inner biopama-tip-inner"></div></div>';
var tipBottomTemplate = '<div class="tooltip biopama-tip-bottom" role="tooltip"><div class="arrow biopama-bottom-arrow"></div><div class="tooltip-inner biopama-tip-inner"></div></div>';
var regionColor = "#a25b28";
var countryColor = "#679B95";
var paColor = "#8FBF4B";


var thisMap;  //the map
var tTipDelay = 200;
var mapContainer = 'map-container';
var mapLoader;
var mapGeneric;
var mapZoom;
var paFill;
var mapInfo;
var mapLegend;

//this is to flag if the user is coming in directly to a country URL to help highlight that country.
var firstVisit = 1;
var regionChanged = 0;
var countryChanged = 0;
var paChanged = 0;

//We create the object that holds our Global settings 
// !!important!! For the country change, as we need to 'fly' to the country we will not always have the needed map features visible
// So we update country settings in 2 parts. First we take the ISO2 code the moment the user changes the setting. THEN we take the other values AFTER the map is done moving
var selSettings = {
    paName: 'default',
	WDPAID: 0,
    countryName: 'trans-ACP',
    regionID: null,
	regionName: null,
	ISO2: null,
	ISO3: null,
	NUM: null,
};
jQuery(window).resize(function(){
	var height = getWindowHeight();
	resizeMap(height);
    if(jQuery(".indicator-chart")){
      myCharts.forEach(function(object, index){
        myCharts[index].resize();
      })
    }
});
jQuery.fn.extend({
	formatNumber: function(options){
		var defaults = {
			cents: '.',
			decimal: ','
			}
		var o =  jQuery.extend(defaults, options);
		return this.each(function() {
			var thiz = jQuery(this), values, x, x1, x2;
				values = jQuery.trim(thiz.html());
				values += '';
				x = values.split(o.cents);
				x1 = x[0];
				x2 = x.length > 1 ? o.cents + x[1] : '';
				var rgx = /(\d+)(\d{3})/;
				while (rgx.test(x1)) {
					x1 = x1.replace(rgx, '$1' + o.decimal + '$2');
				}
				thiz.html(x1 + x2);
		});
	}
});
function precise(x, precision) {
  return Number.parseFloat(x).toFixed(precision);
}
function getWindowHeight(){
	var height = jQuery(window).height();// - $('#admin-menu-wrapper').outerHeight(true) + $('#messages').outerHeight(true);
	if (jQuery('#toolbar-item-administration-tray')[0]){
		var adminHeight = jQuery('#toolbar-item-administration-tray').height() + jQuery('#toolbar-bar').height();
		jQuery('#map-container').css('top', adminHeight);
		height = height - adminHeight;
	}
	return height;
}
function resizeMap(height){
	jQuery('#map-container').css('height', height);
	thisMap.resize();
}
function initiateToolTips(){
  jQuery('.ui-accordion-content a[data-toggle="tooltip"]').tooltip({
    template: tipTopTemplate,
    placement: "top",
    container: '#accordion_right_side',
    delay: 200,
    trigger:"hover",
    html: true,
  });
}