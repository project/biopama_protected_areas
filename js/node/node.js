var countryCharts = [];

var CountrySettings = {
	selIndicators: [],
	selIndicatorMapLayerField: [],
	selIndicatorMappedField: [],
	selIndicatorChartType: [],
	selIndicatorRESTurl: [],
	selIndicatorRESTdataContext: [],
	//dataCountries: [],
	mapLayerField: [],
	mappedField: [],
	mapPoints: [],
	selIndicatorRESTFields: [],
	selIndicatorRESTFieldLabels: [],
	selIndicatorChartProc: [],
	selIndicatorXaxis: [],
	selIndicatorYaxis: [],
	selIndicatorChartSeries: [],
	selIndicatorChartRadarSeries: [],
	selIndicatorChartRadarSettings: [],
	selIndicatorRes: [],
	selIndicatorCountries: [],
	selIndicatorImages: [],
	sort: [],
};

(function ($, Drupal) {
/* 	Drupal.behaviors.refreshView = {
		attach: function (context, settings) {
			$('#drupal-off-canvas').find('form.node-priority-action-form .alert-success, form.node-priority-action-edit-form .alert-success').each( function() {
				$( ".view-priority-actions-regional, .view-priority-actions-country, .view-priority-actions-pa" ).delay( 800 ).trigger('RefreshView');
				$("div.ui-dialog-titlebar button.ui-dialog-titlebar-close").delay( 800 ).trigger('click');
			});
			$('#drupal-off-canvas').find('form.node-management-assessment-justificat-form .alert-success, form.node-priority-action-edit-form .alert-success').each( function() {
				$( ".view-documents-by-scope, .view-management-plans-by-pa" ).delay( 800 ).trigger('RefreshView');
				$("div.ui-dialog-titlebar button.ui-dialog-titlebar-close").delay( 800 ).trigger('click');
			});
		}
	}; */
})(jQuery, Drupal);

jQuery(document).ready(function($) {
	var height = getWindowHeight();
	console.log(height);
	resizeMap(height);
	$('#focus_details_right').css('height', height);
	
	$("div.info-link").on("click", function(e) {
		if ($(this).hasClass("active-chart-button")){
			$(this).removeClass("active-chart-button");
			$(this).closest("div.chart-wrapper").find("div.info-hidden").hide("fast");
		} else {
			$("div.active-chart-button").removeClass('active-chart-button');
			$("div.info-hidden:visible").hide(); //hide any info boxes that may be open
			$(this).addClass("active-chart-button");
			$(this).closest("div.chart-wrapper").find("div.info-hidden").show("fast");
			$("div.chart-large:visible").hide();
		}
	});
	$("div.graph-link").on("click", function(e) {
		if ($(this).hasClass("active-chart-button")){
			$(this).removeClass("active-chart-button");
			$(this).closest("div.chart-wrapper").find("div.chart-large").hide();
		} else {
			$("div.active-chart-button").removeClass("active-chart-button");//rest other chart buttons
			$(this).addClass("active-chart-button"); //activate this one
			$("div.info-hidden:visible").hide(); //hide any info boxes that may be open
			$("div.chart-large").hide(); //hide any other charts that may be open
			$(this).closest("div.chart-wrapper").find("div.chart-large").show(); //show this chart box
			var indicatorID = $(this).closest("div.chart-wrapper").find(".nid").text().trim(); 
			var activeIndicator = $.inArray(indicatorID, CountrySettings.selIndicators);
			sparkLine = false;
			//getCountryRestResults(activeIndicator);
			var indicator = CountrySettings.selIndicators[activeIndicator];
			getCountryChart(activeIndicator, indicator, 0);
		}
	});
	
	$('div.map-link').on("click", function(e) {
		if ($(this).hasClass("active-chart-button")){
			$(this).removeClass("active-chart-button");
			chartLayersToRemove();
		} else {
			$("div.map-link.active-chart-button").removeClass("active-chart-button");
			$(this).addClass("active-chart-button");
			//Find the layers that have been attached to this data by searching up through the tree
			var chartLayers = $(this).closest( ".chart-wrapper" ).find(".chart-layers");
			addChartLayers(chartLayers);
		}
	});

	$( "#accordion" ).accordion({
	  collapsible: true,
	  active: false,
	  heightStyle: "content",
	  activate: checkForCharts, 
	  create: checkForCharts
	});
	//$( "#focus-tabs" ).tabs({heightStyle: "content", activate: changeTheDataAndLayers, create: createIndiCardTabs});
	$( "#focus-tabs" ).tabs({
	  collapsible: true,
	  activate: checkForCharts, 
	  create: checkForCharts
	});
	
	function checkForCharts(){
		//initiateToolTips();
		if ($( ".views-col:visible" ).length){
			$(".views-col:visible").each(function(index) {
				$('a[data-toggle="tooltip"]').tooltip({
					template: tipTopTemplate,
					placement: "top",
					delay: 200,
					trigger:"hover",
					html: true,
				});
				var indicatorName = $.trim($(this).find(".field--name-title").text());
				var indicatorClass = indicatorName.replace(/\s/g, "-");
				var indicatorID = $.trim($(this).find(".nid").text().trim());
				var activeIndicator = $.inArray(indicatorID, CountrySettings.selIndicators);
				if (activeIndicator == -1) {
					CountrySettings.selIndicators.push(indicatorID);
					//for each of these we work our way up from the active url. then find the field we need. This is due to the way drupal views generates the table
					//temp - as not all charts are complete we check if a series AND URL exists to do the chart
					var indicatorRESTurl = $(this).find(".field--name-field-data-rest-url").text();
					CountrySettings.selIndicatorRESTurl.push(indicatorRESTurl);
					
					var indicatorRESTurlContext = $(this).find(".field--name-field-rest-field-context").text();
					CountrySettings.selIndicatorRESTdataContext.push(indicatorRESTurlContext);
					
/* 					if ($(this).find(".field--name-field-data-country").length){
						var dataCountriesArray = [];
						$(this).find( ".field--name-field-data-country" ).children().each(function () {
							dataCountriesArray.push($(this).text().trim())
						});
						CountrySettings.dataCountries.push(dataCountriesArray);
					} */
					
					var mapLayerField = $(this).find( ".field--name-field-data-map-attribute-link.field__item" ).text();
					if ( mapLayerField.length ) CountrySettings.mapLayerField.push(mapLayerField);
					var mappedField = $(this).find( ".field--name-field-data-rest-map-field-link.field__item" ).text();
					if ( mappedField.length ) CountrySettings.mappedField.push(mappedField);
					var mapPoints = $(this).find( ".field--name-field-indicator-make-map-points.field__item" ).text().trim();
					if ( mapPoints.length ) CountrySettings.mapPoints.push(mapPoints);
					var indicatorXaxis = $(this).find(".chart-xaxis").text().trim();
					try {
						indicatorXaxis = JSON.parse(indicatorXaxis);
					} catch (e) {
						indicatorXaxis = '{}';
					}
					CountrySettings.selIndicatorXaxis.push(indicatorXaxis);
					var indicatorYaxis = $(this).find(".chart-yaxis").text().trim();
					try {
						indicatorYaxis = JSON.parse(indicatorYaxis);
					} catch (e) {
						indicatorYaxis = '{}';
					}	
					CountrySettings.selIndicatorYaxis.push(indicatorYaxis);
					if ( $(this).find( ".field--name-field-chart-bl-sort" ).length ){
						CountrySettings.sort.push( $(this).find( ".field--name-field-chart-bl-sort.field__item" ).text().trim() );
					} else {
						CountrySettings.sort.push("none");
					}
					var indicatorSeries = $(this).find(".chart-series").text().trim();
					indicatorSeries = indicatorSeries.replace(/(}\s*\s)+/g, "}||").split("||");
					indicatorSeries.forEach(function(object, index){
						try {
							indicatorSeries[index] = JSON.parse(object);
						} catch (e) {
							indicatorSeries[index] = '{}';
						}	
					});	
					CountrySettings.selIndicatorChartSeries.push(indicatorSeries);
					var indicatorCountries = $(this).find(".indicator-countries").text();
					var indicatorCountriesArray = indicatorCountries.split(", ");
					CountrySettings.selIndicatorCountries.push(indicatorCountriesArray);
					var indicatorImages = $(this).find('img').attr('src');
					CountrySettings.selIndicatorImages.push(indicatorImages);
					var restCheck = $.inArray(indicatorID, CountrySettings.selIndicators);
					if (CountrySettings.selIndicatorRESTurl[restCheck] != null){
						setTimeout(getCountryRestResults(restCheck), 1000)
					}
				} else {
					var restCheck = $.inArray(indicatorID, CountrySettings.selIndicators);
					var indicator = CountrySettings.selIndicators[restCheck];
					setTimeout(getCountryChart(restCheck, indicator, 0), 1000)
				}

			});
		}
		if ($( ".indicator-chart:visible" ).length){
			$( ".indicator-chart:visible" ).each(function( index ) {
			  var chartID = $(this).attr("id");
			  buildIndicatorChart(chartID);
			});
		}
	}
	function buildIndicatorChart(containerID){
		switch(containerID){
			case "pa-avg-climate-chart":
				makeDOPAAvgClimate(containerID);
				break;
			case "pa-dopa-ap":
			case "pa-dopa-roads-pressure":
			case "pa-dopa-roads-in":
			case "pa-dopa-ppi":
			case "pa-dopa-ppi-change":
				//makeDOPAPressureChart(containerID);
				break;
			default:
				break;
		}
	}
	function makeDOPAAvgClimate(containerID){
		var restURL = DOPAavgClimate+selSettings.WDPAID;
		
		jQuery.ajax({
			url: restURL,
			dataType: 'json',
			success: function(d) {
				if (d.metadata.recordCount == 0) {
					jQuery('#'+containerID).text("no records for " + selSettings.paName);
				} else {
					var DOPAChart = echarts.init(document.getElementById(containerID));

					var precip = [];
					var tmax = [];
					var tmin = [];
					var tmean = [];


					jQuery(d.records).each(function(i, data) {
						switch (data.type) {
							case 'prec':
							   for (var prop in data) {
								   if (prop !== 'type' && prop !== 'uom') {
									   precip.push(data[prop])
								   }
							   }
								break;
							case 'tmin':
							   for (var prop in data) {
								   if (prop !== 'type' && prop !== 'uom')
									   tmin.push(data[prop])
							   }
								break;
							case 'tmax':
							   for (var prop in data) {
								   if (prop !== 'type' && prop !== 'uom')
									   tmax.push(data[prop])
							   }
								break;
							case 'tmean':
							   for (var prop in data) {
								   if (prop !== 'type' && prop !== 'uom')
									   tmean.push(data[prop])
							   }
								break;
						   default:
								break;
						}
					});
				}
				
				var option = {
					tooltip: {
						trigger: 'axis',
						axisPointer: {
							type: 'cross',
							crossStyle: {
								color: '#999'
							}
						}
					},
					toolbox: {
						show: true,
						feature: {
							restore: {
								title: 'Restore'
							},
							saveAsImage: {
								title: 'Image',
							}
						}
					},
					//color: {"#bfd4d6"},
					legend: {
						data:['Precipitation','Max Temp.', 'Mean Temp.', 'Min Temp.']
					},
					xAxis: [
						{
							type: 'category',
							data: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
							axisPointer: {
								type: 'shadow'
							}
						}
					],
					yAxis: [
						{
							type: 'value',
							name: 'Precipitation',
							min: 0,
							axisLabel: {
								formatter: '{value} mm'
							}
						},
						{
							type: 'value',
							name: 'Temperature',
							min: 0,
							axisLabel: {
								formatter: '{value} °C'
							}
						}
					],
					series: [
						{
							name:'Precipitation',
							type:'bar',
							data: precip
						},
						{
							name:'Max Temp.',
							type:'line',
							data: tmax,
							yAxisIndex: 1,
							lineStyle: {
								normal: {
									color: '#df9595',
									width: 3,
									type: 'dashed'
								}
							}
						},
						{
							name:'Mean Temp.',
							type:'line',
							yAxisIndex: 1,
							data: tmean,
							lineStyle: {
								normal: {
									color: '#f2cd77',
									width: 3,
									type: 'dashed'
								}
							}
						},
						{
							name:'Min Temp.',
							type:'line',
							yAxisIndex: 1,
							data: tmin,
							lineStyle: {
								normal: {
									color: '#c8dff2',
									width: 3,
									type: 'dashed'
								}
							}
						}
					]
				};
				DOPAChart.setOption(option);
			}
		});	
	}
	function addChartLayers(chartLayers){
		var tabLayerKey;
		chartLayersToRemove();
		if ($(chartLayers).length){
			tabLayerKey = '-b10p4m4';
			//I change the map layer loading to be different
			var mapLayersArray = [];
			var mapLayer;
			$(chartLayers).children().each(function () {
				mapLayer = $(chartLayers).find(".map-layer").text();
				mapLegend = $(chartLayers).find(".map-legend").text();
				try {
					mapLayer = JSON.parse(mapLayer.replace("'", "\""))
					mapLayer.id = mapLayer.id + tabLayerKey;
					//console.log(mapLayer)
					//we need to know if the current source has already been added, and if so make sure it's referenced rather then added again.
					var currentSources = thisMap.style.sourceCaches
					for (var key in currentSources) {
						if (currentSources[key].id == mapLayer.source) {
							mapLayer.source = currentSources[key].id;
						} 
					}
					thisMap.addLayer(mapLayer, 'gaulACP'); 
				} catch (e) {
					console.log("You have a messed up layer for the card")
					mapLayer = '{}'; 
				}	
			});
		}
	}
	function chartLayersToRemove(){
		var currentLayers = thisMap.style._layers;
		for (var key in currentLayers) { 
			if (currentLayers[key].id.indexOf('-b10p4m4') != -1) {
				//add the source of this custom layer to an array. If it's not already in the array
				//we do this to be able to quickly check which sources may still be in use later and remove the unused ones.
				thisMap.removeLayer(currentLayers[key].id);
			} 
		}
		var currentSources = thisMap.style.sourceCaches;
		//so far all the custom sources are being removed regardless of the array defined above... TODO
		for (var key in currentSources) { 
			if (currentSources[key].id.indexOf('-b10p4m4') != -1) {
				thisMap.removeSource(currentSources[key].id);
			} 
		} 
	}
});