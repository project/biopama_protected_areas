<?php
/**
 * @file
 * Contains \Drupal\biopama_protected_areas\Controller\ProtectedAreasController.
 */
 
namespace Drupal\biopama_protected_areas\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines ProtectedAreasController class.
 */
class ProtectedAreasController {
	
  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content() {
    return array(
      '#type' => 'markup',
      '#markup' => t('Hello, World!'),
    );
  }
  
}